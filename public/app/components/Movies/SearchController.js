app.controller('SearchController', function ($scope, $http){

    $scope.searchFilter = {type:"Movie"};
    $scope.search = function() {
      $scope.entities = [];
      $scope.isLoading = true;
      const searchFilter = $scope.searchFilter;

      $http.get(`http://localhost:1234/api/search?name=${searchFilter.name}&type=${searchFilter.type}`)
        .then((entities) => {
        	console.log(entities.data);
          $scope.isLoading = false;
          $scope.entities = entities.data;
        })
        .catch((err) => console.log(err));
		};
});
