const config = require('../config/config.js');
const mongodb = require('mongodb').MongoClient;

class SearchController {

  constructor() {}

  search(req, res) {
    console.log("in controller");
    console.log(req.query)
    mongodb.connect(config.mongourl, function(err, db) {
      if (err){throw err};
      var dbo = db.db("cinema");
      if(req.query.type == null){
        dbo.collection("movies").find({"label" : {$regex : ".*"+ req.query.name+".*", $options:"i"}}).toArray(function(err, result) {
          if (err) throw err;
          res.send(result);
          db.close();
          res.end();
        });
      } else{
        dbo.collection("movies").find({"label" : {$regex : ".*"+ req.query.name+".*", $options:"i"}, "type": req.query.type}).toArray(function(err, result) {
          if (err) throw err;
          res.send(result);
          db.close();
          res.end();
        });
      }

    });
  }

  getCount(req, res){
    mongodb.connect(config.mongourl, function(err, db){
      if(err){throw err};
      var dbo = db.db("cinema");
      dbo.collection("movies").aggregate([{$group:{_id: "$type", count: {$sum: 1}}}]).toArray(function(err, result){
        if(err) throw err;
        res.send(result);
        db.close();
        res.end();

      });
    });
  }
}

module.exports = SearchController;
