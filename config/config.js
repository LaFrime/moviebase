const config = {
  port: 1234,
  mongourl: "mongodb://127.0.0.1:27017/movietracker"
};

module.exports = config;