app.controller('StatsController', function($scope, $http){
  $scope.pieOptions = { legend: {display: true }};
  $scope.barOptions = {scaleShowValues: true,
  scales: {
    yAxes: [{
      ticks: {
        beginAtZero: true
      }
    }],
    xAxes: [{
      ticks: {
        autoSkip: false
      }
    }]
  }}
  $scope.count = {};

  getGenreCount();
  getCount();
  getTopStudios();
  // getTopDirectors();

  function getCount(){
    $http.get(`http://localhost:1234/api/search/count`)
    .then((counts) => {
      for(let countData of counts.data){
        if(countData._id == "Movie"){
          $scope.count.movies = countData.count;
        } else if(countData._id == "Person"){
          $scope.count.persons = countData.count;
        }

      }
    })
    .catch((err) => console.log(err));
  };

  function getGenreCount(){
    $http.get(`http://localhost:1234/api/movies/genre/count`)
    .then((genres) => {
      let graphObj = splitCountValues(genres.data);
      $scope.genreLabels = graphObj.labels
      $scope.genreDatas = graphObj.datas
    })
    .catch((err) => console.log(err));
  };

  function getTopStudios(){
    $http.get(`http://localhost:1234/api/movies/studios/top`)
    .then((studios) => {
      let graphObj = splitCountValues(studios.data);
      $scope.studioLabels = graphObj.labels
      $scope.studioDatas = graphObj.datas
    })
    .catch((err) => console.log(err));
  };

  // function getTopDirectors(){
  //   $http.get(`http://localhost:1234/api/persons/directors/top`)
  //   .then((directors) => {
  //     let graphObj = splitCountValues(directors.data);
  //     $scope.directorsLabels = graphObj.labels
  //     $scope.directorsDatas = graphObj.datas
  //   })
  //   .catch((err) => console.log(err));
  // };

  function splitCountValues(values){
    let labels = [];
    let datas = [];
    for(value of values){
      if(value._id != null){
      labels.push(value._id);
      datas.push(value.count);
    }
    }
    return {labels, datas};
  }
});
