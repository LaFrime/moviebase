app.controller('PersonController', function($scope, $http){
	id = getParameterByName('id');
	getPerson(id);
	$scope.isLoading = true;
	function getPerson(id) {
		$http.get(`http://localhost:1234/api/persons/${id}`)
		.then((person) => {
			$scope.person = person.data;
			getMoviesByKey($scope.person._key);
		})
		.catch((err) => console.log(err));
	};

	function getMoviesByKey(key) {
		$http.get(`http://localhost:1234/api/persons/movies/${key}`)
		.then((movies) => {
			let actor = [];
			let director = [];
			let keys = [];
			for(movie of movies.data){
				console.log(movie)
				if(!keys.includes(movie._key)){
					if(movie.label == "ACTS_IN"){
						actor.push(movie);
					} else if(movie.label == "DIRECTED"){
						director.push(movie)
					}
					keys.push(movie._key);
				}
			}
			$scope.person.actor = actor;
			$scope.person.director = director;
			console.log($scope.person.actor);
			$scope.isLoading = false;
		})
		.catch((err) => console.log(err));
	};

	function getParameterByName(name, url) {
		if (!url) {
			url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
});
