const bodyParser = require('body-parser');
const express = require('express');

const config = require('./config/config.js');
const mongoMiddleware = require('./middlewares/mongoMiddleware.js');
const SearchController = require('./controllers/SearchController.js');
const MoviesController = require('./controllers/MoviesController.js');
const PersonsController = require('./controllers/PersonsController.js');

const mongodb = require('mongodb').MongoClient;


const app = express();
const searchController = new SearchController();
const moviesController = new MoviesController();
const personsController = new PersonsController();

app.use(mongoMiddleware(config.mongourl));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.static("public"));

app.get('/api/search', searchController.search.bind(searchController));
app.get('/api/search/count', searchController.getCount.bind(searchController));

app.get('/api/persons/:id', personsController.getById.bind(personsController));
app.get("/api/persons/movies/:key", personsController.getMoviesByKey.bind(personsController));

app.get('/api/movies/:id', moviesController.getById.bind(moviesController));
app.post('/api/movies/:id/comments', moviesController.postComment.bind(moviesController));
app.get("/api/movies/genre/count", moviesController.getGenreCount.bind(moviesController));
app.get("/api/movies/studios/top", moviesController.getTopStudios.bind(moviesController));
app.get("/api/persons/directors/top", personsController.getTopDirectors.bind(personsController));
app.delete("/api/movies/:id/comments/:commentId", moviesController.deleteComment.bind(moviesController))


app.get('*', function(req, res){
        res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
});

app.listen(config.port, (err) => {
  if (err) throw err;
  console.log(`Listening on port ${config.port}`);
});
