app.controller('MovieController', function($scope, $http){
	id = getParameterByName('id');
	$scope.newComment = {note: "5"};
	getMovie(id);
	function getMovie(id) {
		$http.get(`http://localhost:1234/api/movies/${id}`)
		.then((movie) => {
			$scope.movie = movie.data;
			console.log($scope.movie)
			if($scope.movie.comments == null){
				$scope.movie.comments = [];
			}
		})
		.catch((err) => console.log(err));
	};
	function getParameterByName(name, url) {
		if (!url) {
			url = window.location.href;
		}
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}


	function getVideoId(url){
		console.log(url)
		var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
		var match = url.match(regExp);

		if (match && match[2].length == 11) {
			console.log(match[2]);
			return match[2];
		}
	}

	$scope.sendComment = function(){
		console.log($scope.newComment);
		$http.post(`http://localhost:1234/api/movies/${id}/comments`, $scope.newComment).then((comment) => {
			console.log("successfuly sent")
			$scope.isCommentSent = true;
			$scope.movie.comments.push($scope.newComment);

		})
		.catch((err) => console.log(err));
	}

	// $scope.deleteComment = function(comment){
	// 	$http.delete(`http://localhost:1234/api/movies/${id}/comments/${comment.id}`).then((comment) => {
	// 		console.log("successfuly sent")
	// 	})
	// 	.catch((err) => console.log(err));
	// }
	$scope.getNoteAverage = function(comments) {
		let sum = 0;
		if(comments == null){
			return "?";
		}
		for(let comment of comments){
			console.log(comment.note);
			if(comment.note != null){
				sum += parseInt( comment.note, 10 );
			}
		}

		if(sum != null){
			let avg = sum/comments.length;
			return Number(Math.round(avg+'e1')+'e-1');
		} else{
			return "?";
		}

	}

	$scope.formatDate = function(date){
		let formatDate = new Date(date * 1);
		return formatDate.toLocaleDateString("en-EN");
	}
});
