const config = require('../config/config.js');
const mongodb = require('mongodb');

class MoviesController {

  constructor() {}

  getById(req, res) {
    console.log(req.params.id);
    if (!req.params.id) {
      return res.status(400).json("Missing 'id' parameter");
    }
    mongodb.MongoClient.connect(config.mongourl, function(err, db) {
      if (err){throw err};
      var dbo = db.db("cinema");
      dbo.collection("movies").findOne({"id":req.params.id, "type": "Movie"}, function(err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result);
        db.close();
        res.end();
      });
    });
}
postComment(req, res) {
  console.log(req.params.id);
  console.log(req.body);
  if (!req.params.id) {
    return res.status(400).json("Missing 'id' parameter");
  }
  mongodb.MongoClient.connect(config.mongourl, function(err, db) {
    if (err){throw err};
    var dbo = db.db("cinema");
    dbo.collection("movies").update({"id":req.params.id,"type":"Movie"}, {$push : {comments : req.body}}, function(err, result) {
      if (err) throw err;
      res.send(result);
      db.close();
      res.end();
    });
  });
}

deleteComment(req, res) {
  console.log(req.params.id);
  console.log(req.body);
  if (!req.params.id) {
    return res.status(400).json("Missing 'id' parameter");
  }
  mongodb.MongoClient.connect(config.mongourl, function(err, db) {
    if (err){throw err};
    var dbo = db.db("cinema");
    dbo.collection("movies").updateOne({"id":req.params.id}, {$pull : {comments: {id:req.params.commentId}}}, function(err, result) {
      if (err) throw err;
      res.send(result);
      db.close();
      res.end();
    });
  });
}


getGenreCount(req, res){
  mongodb.MongoClient.connect(config.mongourl, function(err, db){
    if(err){throw err};
    var dbo = db.db("cinema");
    dbo.collection("movies").aggregate([{$match:{"type":"Movie"}},{$group:{_id: "$genre", count: {$sum: 1}}}]).toArray(function(err, result){
      if(err) throw err;
      res.send(result);
      db.close();
      res.end();

    });
  });
}

getTopStudios(req, res){
  mongodb.MongoClient.connect(config.mongourl, function(err, db){
    if(err){throw err};
    var dbo = db.db("cinema");
    dbo.collection("movies").aggregate([{$match:{"type":"Movie"}},{$group:{_id: "$studio", count: {$sum: 1}}}, {$sort:{count:-1}}, {$limit: 15}]).toArray(function(err, result){
      if(err) throw err;
      res.send(result);
      db.close();
      res.end();

    });
  });
}
}

module.exports = MoviesController;
