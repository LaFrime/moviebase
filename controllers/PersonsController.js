const config = require('../config/config.js');
const mongodb = require('mongodb');


class PersonsController {

constructor() {}



getById(req, res) {
  console.log(req.params.id);
  if (!req.params.id) {
    return res.status(400).json("Missing 'id' parameter");
  }
  mongodb.MongoClient.connect(config.mongourl, function(err, db) {
    if (err){throw err};
    var dbo = db.db("cinema");
    dbo.collection("movies").findOne({"id":req.params.id, type:"Person"}, function(err, result) {
      if (err) throw err;
      console.log(result);
      res.send(result);
      db.close();
      res.end();
    });
  });
}

getMoviesByKey(req, res) {
  if(!req.params.key){
    return res.status(400).json("Missing 'key' parameter");
  }

  mongodb.MongoClient.connect(config.mongourl, function(err, db) {
    if (err){throw err};
    var dbo = db.db("cinema");
    dbo.collection('links').aggregate([{$lookup: {from: "movies", localField: "_to", foreignField: "_key", as: "movie"}}, {$match:{"_from":req.params.key}}]).toArray(function(err, result){
      if (err) throw err;
      console.log(result);
      res.send(result);
      db.close();
      res.end();
    });
  });
}

getTopDirectors(req, res) {
  if(!req.params.key){
    return res.status(400).json("Missing 'key' parameter");
  }

  mongodb.MongoClient.connect(config.mongourl, function(err, db) {
    if (err){throw err};
    var dbo = db.db("cinema");
    dbo.collection('links').aggregate([{$match:{"label":"DIRECTED"}},{$group:{_id: "$_from", count: {$sum: 1}}},{$lookup: {from: "movies", localField: "_from", foreignField: "_key", as: "person"}},{$sort:{count:-1}}, {$limit: 15}]).toArray(function(err, result){
      if (err) throw err;
      console.log(result);
      res.send(result);
      db.close();
      res.end();
    });
  });
}
}



module.exports = PersonsController;
